#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:7eb82179d517ef65c7abdffb891bd4289e043803; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:2d854ec66c24e8d4bea0ed0275cccc22f9dd5aa9 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:7eb82179d517ef65c7abdffb891bd4289e043803 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
