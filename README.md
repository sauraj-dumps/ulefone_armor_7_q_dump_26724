## full_k79v1_64-user 10 QP1A.190711.020 1605756878 release-keys
- Manufacturer: ulefone
- Platform: mt6779
- Codename: Armor_7_Q
- Brand: Ulefone
- Flavor: full_k79v1_64-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 1605756878
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Armor_7_Q_EEA/Armor_7_Q:10/QP1A.190711.020/1605756878:user/release-keys
- OTA version: 
- Branch: full_k79v1_64-user-10-QP1A.190711.020-1605756878-release-keys
- Repo: ulefone_armor_7_q_dump_26724


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
